# Translation of kcm_nic to Norwegian Bokmål
#
# Bjørn Steensrud <bjornst@skogkatt.homelinux.org>, 2002, 2007, 2010.
msgid ""
msgstr ""
"Project-Id-Version: kcmnic\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-05 02:14+0000\n"
"PO-Revision-Date: 2010-11-21 20:16+0100\n"
"Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>\n"
"Language-Team: Norwegian Bokmål <l10n-no@lister.huftis.org>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.1\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: networkmodel.cpp:161
#, kde-format
msgctxt "@item:intable Network type"
msgid "Point to Point"
msgstr ""

#: networkmodel.cpp:168
#, kde-format
msgctxt "@item:intable Network type"
msgid "Broadcast"
msgstr ""

#: networkmodel.cpp:175
#, kde-format
msgctxt "@item:intable Network type"
msgid "Multicast"
msgstr ""

#: networkmodel.cpp:182
#, kde-format
msgctxt "@item:intable Network type"
msgid "Loopback"
msgstr ""

#: ui/main.qml:29
#, kde-format
msgctxt "@label"
msgid "Name:"
msgstr ""

#: ui/main.qml:33
#, kde-format
msgctxt "@label"
msgid "Address:"
msgstr ""

#: ui/main.qml:37
#, kde-format
msgctxt "@label"
msgid "Network Mask:"
msgstr ""

#: ui/main.qml:41
#, kde-format
msgctxt "@label"
msgid "Type:"
msgstr ""

#: ui/main.qml:45
#, kde-format
msgctxt "@label"
msgid "Hardware Address:"
msgstr ""

#: ui/main.qml:50
#, kde-format
msgctxt "@label"
msgid "State:"
msgstr ""

#: ui/main.qml:58
#, kde-format
msgctxt "State of network card is connected"
msgid "Up"
msgstr "Oppe"

#: ui/main.qml:59
#, kde-format
msgctxt "State of network card is disconnected"
msgid "Down"
msgstr "Nede"

#: ui/main.qml:70
#, kde-format
msgctxt "@action:button"
msgid "Refresh"
msgstr ""
